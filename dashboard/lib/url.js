/**
 * A basic URL constructor to make it easier to construct URLs with search
 * parameters.
 *
 * @param {string} originAndPath The main URL (origin and pathname)
 * @param {Object} params The search parameters to add. Keys are names, values
 *     are values.
 * @returns {string} The constructed URL with correctly escaped search
 *     parameters.
 */
export function makeUrl(baseAndPath, params = {}) {
  const url = new URL(baseAndPath);
  Object.entries(params).forEach(([name, value]) => {
    url.searchParams.set(name, value);
  });

  return url.href;
}
