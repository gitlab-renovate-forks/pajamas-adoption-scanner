import { getFilterGroups, getExplicitFilterGroups } from './scim2';

describe('getFilterGroups', () => {
  it('returns empty array given an unparseable query', () => {
    expect(getFilterGroups('foo')).toEqual([]);
  });

  it.each([undefined, null, '', '  '])(
    'returns single group with filter that matches anything given %p',
    (query) => {
      expect(getFilterGroups(query)).toEqual([
        {
          filter: expect.any(Function),
          name: 'Anything and everything',
        },
      ]);
    },
  );

  // See https://github.com/thomaspoignant/scim2-parse-filter/pull/151
  it('handles "not" filters', () => {
    expect(getFilterGroups('not a pr')).toEqual([
      {
        filter: expect.any(Function),
        name: 'not (a pr)',
      },
    ]);
  });

  it('returns single group for queries without ors', () => {
    const result = getFilterGroups('foo eq "bar"');
    expect(result).toEqual([
      {
        filter: expect.any(Function),
        name: 'foo eq "bar"',
      },
    ]);
    expect(result[0].filter({ foo: 'bar' })).toBe(true);
    expect(result[0].filter({ foo: 'qux' })).toBe(false);

    const result2 = getFilterGroups('foo eq "bar" and bar pr');
    expect(result2).toEqual([
      {
        filter: expect.any(Function),
        name: 'foo eq "bar" and bar pr',
      },
    ]);
    expect(result2[0].filter({ foo: 'bar', bar: 1 })).toBe(true);
    expect(result2[0].filter({ foo: 'bar' })).toBe(false);
    expect(result2[0].filter({ bar: 1 })).toBe(false);
  });

  it('returns expanded (no []) name', () => {
    const result = getFilterGroups('foo[bar eq "bar"]');
    expect(result).toEqual([
      {
        filter: expect.any(Function),
        name: 'foo.bar eq "bar"',
      },
    ]);
    expect(result[0].filter({ foo: { bar: 'bar' } })).toBe(true);
    expect(result[0].filter({ fo: { bar: 'bar' } })).toBe(false);
    expect(result[0].filter({ foo: { ba: 'bar' } })).toBe(false);
  });

  it('returns multiple groups for queries with ors', () => {
    const result = getFilterGroups('a eq 1 or b gt 2');
    expect(result).toEqual([
      {
        filter: expect.any(Function),
        name: 'a eq 1',
      },
      {
        filter: expect.any(Function),
        name: 'b gt 2',
      },
    ]);
    expect(result[0].filter({ a: 1 })).toBe(true);
    expect(result[0].filter({ b: 3 })).toBe(false);
    expect(result[1].filter({ a: 1 })).toBe(false);
    expect(result[1].filter({ b: 3 })).toBe(true);
  });

  it('correctly handles a complex query', () => {
    const result = getFilterGroups('a eq 1 and (b eq 2 or (c eq 3 and x[d eq 4 or e eq 5]))');
    expect(result).toEqual([
      {
        filter: expect.any(Function),
        name: 'a eq 1 and b eq 2',
      },
      {
        filter: expect.any(Function),
        name: 'a eq 1 and c eq 3 and x.d eq 4',
      },
      {
        filter: expect.any(Function),
        name: 'a eq 1 and c eq 3 and x.e eq 5',
      },
    ]);
    const matchesFirst = { a: 1, b: 2 };
    const matchesSecond = { a: 1, c: 3, x: { d: 4 } };
    const matchesThird = { a: 1, c: 3, x: { e: 5 } };
    expect(result[0].filter(matchesFirst)).toBe(true);
    expect(result[0].filter(matchesSecond)).toBe(false);
    expect(result[0].filter(matchesThird)).toBe(false);
    expect(result[1].filter(matchesFirst)).toBe(false);
    expect(result[1].filter(matchesSecond)).toBe(true);
    expect(result[1].filter(matchesThird)).toBe(false);
    expect(result[2].filter(matchesFirst)).toBe(false);
    expect(result[2].filter(matchesSecond)).toBe(false);
    expect(result[2].filter(matchesThird)).toBe(true);
  });
});

describe('getExplicitFilterGroups', () => {
  it('returns empty array given an unparseable query', () => {
    expect(getExplicitFilterGroups('foo')).toEqual([]);
  });

  it.each([undefined, null, '', '  '])(
    'returns single group with filter that matches anything given %p',
    (query) => {
      expect(getExplicitFilterGroups(query)).toEqual([
        {
          filter: expect.any(Function),
          name: 'Anything and everything',
        },
      ]);
    },
  );

  it('returns single group for queries with no outermost or', () => {
    const result = getExplicitFilterGroups('foo eq "bar"');
    expect(result).toEqual([
      {
        filter: expect.any(Function),
        name: 'foo eq "bar"',
      },
    ]);
    expect(result[0].filter({ foo: 'bar' })).toBe(true);
    expect(result[0].filter({ foo: 'qux' })).toBe(false);

    const result2 = getExplicitFilterGroups('foo eq "bar" and bar pr');
    expect(result2).toEqual([
      {
        filter: expect.any(Function),
        name: 'foo eq "bar" and bar pr',
      },
    ]);
    expect(result2[0].filter({ foo: 'bar', bar: 1 })).toBe(true);
    expect(result2[0].filter({ foo: 'bar' })).toBe(false);
    expect(result2[0].filter({ bar: 1 })).toBe(false);
  });

  it('returns multiple groups for queries with outermost ors', () => {
    const result = getExplicitFilterGroups('a eq 1 or b gt 2');
    expect(result).toEqual([
      {
        filter: expect.any(Function),
        name: 'a eq 1',
      },
      {
        filter: expect.any(Function),
        name: 'b gt 2',
      },
    ]);
    expect(result[0].filter({ a: 1 })).toBe(true);
    expect(result[0].filter({ b: 3 })).toBe(false);
    expect(result[1].filter({ a: 1 })).toBe(false);
    expect(result[1].filter({ b: 3 })).toBe(true);
  });

  it('correctly handles a complex query', () => {
    const result = getExplicitFilterGroups(
      '(a eq 1 or b eq 2) or (a eq 2 and x[c eq 3]) or d eq 4',
    );
    expect(result).toEqual([
      {
        filter: expect.any(Function),
        name: 'a eq 1 or b eq 2',
      },
      {
        filter: expect.any(Function),
        name: 'a eq 2 and x[c eq 3]',
      },
      {
        filter: expect.any(Function),
        name: 'd eq 4',
      },
    ]);
    const matchesFirstA = { a: 1 };
    const matchesFirstB = { b: 2 };
    const matchesSecond = { a: 2, x: { c: 3 } };
    const matchesThird = { d: 4 };
    expect(result[0].filter(matchesFirstA)).toBe(true);
    expect(result[0].filter(matchesFirstB)).toBe(true);
    expect(result[0].filter(matchesSecond)).toBe(false);
    expect(result[0].filter(matchesThird)).toBe(false);
    expect(result[1].filter(matchesFirstA)).toBe(false);
    expect(result[1].filter(matchesFirstB)).toBe(false);
    expect(result[1].filter(matchesSecond)).toBe(true);
    expect(result[1].filter(matchesThird)).toBe(false);
    expect(result[2].filter(matchesFirstA)).toBe(false);
    expect(result[2].filter(matchesFirstB)).toBe(false);
    expect(result[2].filter(matchesSecond)).toBe(false);
    expect(result[2].filter(matchesThird)).toBe(true);
  });

  it('treats [] as a bracketed subfilter', () => {
    const result = getExplicitFilterGroups(
      'check_id pr or extra[severity eq "ERROR" or severity eq "WARNING"]',
    );
    expect(result).toEqual([
      {
        filter: expect.any(Function),
        name: 'check_id pr',
      },
      {
        filter: expect.any(Function),
        name: 'extra[severity eq "ERROR" or severity eq "WARNING"]',
      },
    ]);
  });
});
