import { ALL } from '../components/by_component.vue';
import { COLOR_INFO, COLOR_WARNING, COLOR_ERROR } from './constants';

const filteredHistoryToStackedColumnChartProps = (history, rules, rulePredicate) => {
  return history.reduce(
    (acc, scan, i) => {
      acc.bars[0].data[i] = 0;
      acc.bars[1].data[i] = 0;
      acc.bars[2].data[i] = 0;
      acc.groupBy[i] = scan.date;

      Object.entries(scan.summary).forEach(([id, count]) => {
        const rule = rules.get(id);

        if (!rule) {
          // This can happen if there's a mismatch between the scan results and
          // the combined rules file. This *should* only be possible in
          // development if one was updated without the other.
          // eslint-disable-next-line no-console
          console.warn(`Invalid history: non-existent rule with id "${id}"`);
          return;
        }

        if (!rulePredicate(rule)) return;

        const barIndex = ['INFO', 'WARNING', 'ERROR'].indexOf(rule.severity);
        acc.bars[barIndex].data[i] += count;
      });

      return acc;
    },
    {
      bars: [
        // The order of these determines their color, based on customPalette.
        { name: 'Adopted', data: [] },
        { name: 'Warnings', data: [] },
        { name: 'Not adopted', data: [] },
      ],
      groupBy: [],
      xAxisType: 'category',
      xAxisTitle: 'Date',
      yAxisTitle: 'Findings',
      customPalette: [COLOR_INFO, COLOR_WARNING, COLOR_ERROR],
    },
  );
};

/**
 * Transforms a history array to props for the GlStackedColumnChart component.
 *
 * @param {Array} history Array summarizing multiple semgrep scans.
 * @param {Map} rules Array of rule definitions.
 * @param {String} componentLabel Component label to filter by.
 * @returns {Object}
 */
export const historyToStackedColumnChartProps = (history, rules, componentLabel) => {
  const rulePredicate =
    componentLabel === ALL
      ? () => true
      : ({ metadata }) => metadata.componentLabel === componentLabel;

  return filteredHistoryToStackedColumnChartProps(history, rules, rulePredicate);
};
