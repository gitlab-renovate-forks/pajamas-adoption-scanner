import { historyToStackedColumnChartProps } from './history';
import { getHistory, getRules } from '~/test_support/mock_scans';
import { ALL } from '~/components/by_component.vue';

describe('historyToStackedColumnChartProps', () => {
  it('returns correct props for unfiltered history', () => {
    expect(historyToStackedColumnChartProps(getHistory(), getRules(), ALL)).toEqual({
      groupBy: ['2021-01-01', '2022-02-02'],
      bars: [
        { name: 'Adopted', data: [1, 3] },
        { name: 'Warnings', data: [5, 7] },
        { name: 'Not adopted', data: [2, 4] },
      ],
      xAxisType: 'category',
      xAxisTitle: 'Date',
      yAxisTitle: 'Findings',
      customPalette: [expect.any(String), expect.any(String), expect.any(String)],
    });
  });

  it('returns correct props for a particular componentLabel', () => {
    expect(historyToStackedColumnChartProps(getHistory(), getRules(), 'component:button')).toEqual({
      groupBy: ['2021-01-01', '2022-02-02'],
      bars: [
        { name: 'Adopted', data: [1, 1] },
        { name: 'Warnings', data: [0, 0] },
        { name: 'Not adopted', data: [1, 2] },
      ],
      xAxisType: 'category',
      xAxisTitle: 'Date',
      yAxisTitle: 'Findings',
      customPalette: [expect.any(String), expect.any(String), expect.any(String)],
    });
  });

  it('returns correct props when there is no history for a particular componentLabel', () => {
    expect(historyToStackedColumnChartProps(getHistory(), getRules(), 'component:tree')).toEqual({
      groupBy: ['2021-01-01', '2022-02-02'],
      bars: [
        { name: 'Adopted', data: [0, 0] },
        { name: 'Warnings', data: [0, 0] },
        { name: 'Not adopted', data: [0, 0] },
      ],
      xAxisType: 'category',
      xAxisTitle: 'Date',
      yAxisTitle: 'Findings',
      customPalette: [expect.any(String), expect.any(String), expect.any(String)],
    });
  });
});
