import { getCurrentScan } from './api';
import { getTypicalScan, getScanSummary } from '~/test_support/mock_scans';

const mockFetchResponses = (pathToDataMap) => {
  global.fetch = jest.fn((path) => {
    const data = pathToDataMap[path];
    if (!data) {
      return Promise.reject(new Error(`Unexpected request to ${path}`));
    }

    return Promise.resolve({
      ok: true,
      json: () => Promise.resolve(data),
    });
  });
};

describe('getCurrentScan', () => {
  it('adds a unique id to each finding', async () => {
    const scans = getScanSummary();

    mockFetchResponses({
      'scans.json': scans,
      [scans.current.file]: getTypicalScan(),
    });

    const scan = await getCurrentScan();
    const ids = new Set(scan.results.map(({ id }) => id));

    expect(ids.size).toBe(scan.results.length);
  });
});
