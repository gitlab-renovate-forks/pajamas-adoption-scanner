import { filter, parse, stringify } from 'scim2-parse-filter';

const returnTrue = () => true;

export const getScim2Filter = (query) => {
  let parseError = '';
  let filterFn = returnTrue;

  if (query.trim()) {
    try {
      filterFn = filter(parse(query));
    } catch (e) {
      parseError = `Invalid filter: ${e.message}`;
    }
  }

  return {
    filterFn,
    parseError,
  };
};

function baseProduct(previousProduct, b) {
  const result = [];

  for (const x of previousProduct) {
    for (const y of b) {
      result.push([...x, y]);
    }
  }

  return result;
}

function product(arrays) {
  const [first, ...rest] = arrays;

  let result = first.map((v) => [v]);

  for (const array of rest) {
    result = baseProduct(result, array);
  }

  return result;
}

function isOr(f) {
  return f.op === 'or';
}

function expand(f) {
  switch (f.op) {
    case 'or':
      return { ...f, filters: f.filters.map(expand) };
    case 'and': {
      const expandedChildren = f.filters.map(expand);
      const hasOr = expandedChildren.some(isOr);

      if (!hasOr) return { op: 'and', filters: expandedChildren };

      const distributedAnds = product(
        expandedChildren.map((child) => (child.op === 'or' ? child.filters : [child])),
      ).map((filters) => expand({ op: 'and', filters }));

      return { op: 'or', filters: distributedAnds };
    }
  }
  return f;
}

// Begin copy of functions from https://github.com/thomaspoignant/scim2-parse-filter/pull/151
const valfilter = (f, path) => {
  if (path && 'attrPath' in f) {
    f = { ...f, attrPath: `${path}.${f.attrPath}` };
  }
  switch (f.op) {
    case 'and':
    case 'or':
      return { ...f, filters: f.filters.map((c) => valfilter(c, path)) };
    case 'not':
      return { ...f, filter: valfilter(f.filter, path) };
    case '[]':
      return valfilter(f.valFilter, f.attrPath);
  }
  return f;
};

// 1 and 2 or (1 or b) => 1 and 2 or 1 or b
export const log = (f) => {
  switch (f.op) {
    case 'and':
    case 'or': {
      const filters = f.filters.map(log);
      const result = [];
      filters.forEach((c) => {
        if (c.op === f.op) {
          c.filters.forEach((cc) => result.push(cc));
        } else {
          result.push(c);
        }
      });
      return { ...f, filters: result };
    }
  }
  return f;
};

function flatten(f) {
  return log(valfilter(f));
}
// End copy of functions from https://github.com/thomaspoignant/scim2-parse-filter/pull/151

export function getFilterGroupsBase(astTransformer, query) {
  if (!query || !query.trim())
    return [
      {
        filter: returnTrue,
        name: 'Anything and everything',
      },
    ];

  let parsed;

  try {
    parsed = parse(query);
  } catch {
    return [];
  }

  const transformed = astTransformer(parsed);

  if (!isOr(transformed)) {
    return [
      {
        filter: filter(transformed),
        name: stringify(transformed),
      },
    ];
  }

  return transformed.filters.map((f) => ({
    filter: filter(f),
    name: stringify(f),
  }));
}

export function getFilterGroups(query) {
  return getFilterGroupsBase((parsed) => flatten(expand(flatten(parsed))), query);
}

export function getExplicitFilterGroups(query) {
  return getFilterGroupsBase((parsed) => parsed, query);
}
