import memoize from 'lodash/memoize';

const SessionStorageBackedAsyncJSONCache = !process.client
  ? memoize.Cache
  : class SessionStorageBackedAsyncJSONCache extends Map {
      constructor(...args) {
        super(...args);

        this._scheduledWrite = null;
        this._restoreFromSessionStorage();
      }

      _restoreFromSessionStorage() {
        for (let i = 0; i < sessionStorage.length; i += 1) {
          const key = sessionStorage.key(i);

          try {
            const value = JSON.parse(sessionStorage.getItem(key));
            super.set(key, Promise.resolve(value));
          } catch (error) {
            console.warn(`Failed to load from "${key}" in session storage; deleting it.`);
            sessionStorage.removeItem(key);
          }
        }
      }

      _scheduleWriteToSessionStorage() {
        if (this._scheduledWrite != null) return;

        this._scheduledWrite = setTimeout(async () => {
          try {
            sessionStorage.clear();
            for (const [key, value] of this.entries()) {
              sessionStorage.setItem(key, JSON.stringify(await value));
            }
          } catch (error) {
            console.warn('Failure while writing to session storage', error);
          } finally {
            this._scheduledWrite = null;
          }
        }, 5000);
      }

      get(key) {
        return super.get(JSON.stringify(key));
      }

      set(key, value) {
        this._scheduleWriteToSessionStorage();
        return super.set(JSON.stringify(key), value);
      }

      has(key) {
        return super.has(JSON.stringify(key));
      }

      delete(key) {
        this._scheduleWriteToSessionStorage();
        return super.delete(JSON.stringify(key));
      }

      clear() {
        this._scheduleWriteToSessionStorage();
        return super.clear();
      }
    };

export { memoize };

export function memoizeBackedBySessionStorage(...args) {
  const originalCache = memoize.Cache;
  memoize.Cache = SessionStorageBackedAsyncJSONCache;
  const memoized = memoize(...args);
  memoize.Cache = originalCache;

  return memoized;
}
