# This file defines shared options, variables and functions for scripts. It is
# not meant to be called directly (hence it is not executable).
#
# Ignore unused variables; they are used in scripts that source this one.
# shellcheck disable=SC2034
# shellcheck shell=bash

set -euo pipefail
IFS=$'\n\t'

ROOT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")/.." &>/dev/null && pwd)

CI="${CI:-}"

REPO="$ROOT_DIR/${GITLAB_CLONE_DIR:-gitlab}"
MAIN_BRANCH=master
ORIGINAL_REF=

export SEMGREP_SEND_METRICS=off
export SEMGREP_ENABLE_VERSION_CHECK=0

mkdir -p "$ROOT_DIR/tmp/history"

cyan() {
  printf "\033[;36m%s\033[0m" "$@"
}

warn() {
  printf >&2 '%s\n' "$*"
}

fail() {
  warn "$*"
  exit 1
}

section_start() {
  local section_title="${1}"
  local section_description="${2:-$section_title}"
  local collapsed="${3:-true}"

  echo -e "section_start:$(date +%s):${section_title}[collapsed=${collapsed}]\r\e[0K$(cyan "${section_description}")"
}

section_end() {
  local section_title="${1}"

  echo -e "section_end:$(date +%s):${section_title}\r\e[0K"
}

common_help() {
  cat <<HEREDOC
To run this locally, first symlink the GitLab repository to "$REPO". \
For example:

    ln -s <path_to_gitlab_repo> $REPO

OPTIONS
  -h, --help     print this help message
HEREDOC
}

ensure_gitlab_repo() {
  if [ -n "$CI" ]; then
    if [ -d "$REPO" ]; then
      warn "In CI, using existing clone at $REPO"
    else
      section_start "clone_gitlab" "Cloning gitlab@${MAIN_BRANCH} into $REPO"

      set -x
      git clone https://gitlab.com/gitlab-org/gitlab.git \
        "$REPO" \
        "$@" \
        --branch "$MAIN_BRANCH"
      set +x

      section_end "clone_gitlab"
    fi

    cd "$REPO"
  else
    warn "Not running in CI, so assuming gitlab dir/link already exists..."

    cd "$REPO" || fail "Have you run \`ln -s <path_to_gitlab> gitlab\`?"

    local status
    status=$(git status --porcelain)
    if [ -n "$status" ]; then
      warn "The GitLab repository isn't clean. Please commit or stash the changes listed before trying again."
      fail "$status"
    fi

    ORIGINAL_REF="$(git rev-parse --symbolic --abbrev-ref @)"
    warn "Stored original ref $ORIGINAL_REF"
    git fetch origin "$MAIN_BRANCH"
  fi
}

# TODO: Find a better way of getting 6 months of history of the main branch.
# Using --shallow-since="6 months" doesn't work well due to long-lived feature
# branches.
#
# We may want to occasionally bump the tag here to keep the clone "small".
ensure_gitlab_repo_with_history() {
  ensure_gitlab_repo --shallow-exclude="${CI_CLONE_EXCLUDE_FROM_TAG:-v14.8.0-ee}"
}

attempt_to_restore_original_git_checkout() {
  [ -z "$ORIGINAL_REF" ] && return 0

  cd "$REPO"
  warn "Restoring repo checkout to $ORIGINAL_REF..."
  git checkout "$ORIGINAL_REF" || true
}
