#!/usr/bin/env node
/* eslint-disable no-console */

import { groupForPath } from '../lib/groups.js';

const ANSI_RED = '\u001b[31;9m';
const ANSI_GREEN = '\u001b[32m';
const ANSI_RESET = '\u001b[0;m';

const GROUP_CHANGES_LABEL = 'pipeline:expect-group-changes';

const BLOCKING_MESSAGE = `

If the group changes above are what you expect, add the
"${GROUP_CHANGES_LABEL}" label to this merge request, and then retry
this job.

For help, contact the Manage:Foundations team.
`;

const CI = 'CI' in process.env;
const expectingGroupChanges = (process.env.CI_MERGE_REQUEST_LABELS ?? '').includes(
  GROUP_CHANGES_LABEL,
);

/**
 * Utility to work around https://gitlab.com/gitlab-org/gitlab/-/issues/217231.
 *
 * Works by adding whitespace to otherwise blank lines.
 *
 * Editors/prettier remove trailing whitespace, so it's tricky and non-explicit
 * to directly add these in strings. This approach is explicit.
 */
function preserveBlankLines(string) {
  return string.replace(/\n(?=\n)/g, '\n ');
}

async function readStdin() {
  if (process.stdin.isTTY) {
    throw new Error('Pipe a Semgrep JSON scan file into this script.');
  }

  const chunks = [];

  for await (const chunk of process.stdin) {
    chunks.push(chunk);
  }

  return Buffer.concat(chunks).toString();
}

function sortByPath(a, b) {
  if (a.path > b.path) return 1;
  if (a.path < b.path) return -1;
  return 0;
}

function logDiff(path, oldGroup, newGroup) {
  console.warn(
    `${path}: ${ANSI_RED}${oldGroup}${ANSI_RESET} ${ANSI_GREEN}${newGroup}${ANSI_RESET}`,
  );
}

async function main() {
  const input = await readStdin();
  const scan = JSON.parse(input);

  const old = scan.results.sort(sortByPath).reduce((acc, result) => {
    if (!acc.has(result.path)) {
      acc.set(result.path, result.extra.group);
    }

    return acc;
  }, new Map());

  let total = 0;

  for (const [path, oldGroup] of old) {
    const newGroup = groupForPath(path);

    if (newGroup !== oldGroup) {
      total += 1;
      logDiff(path, oldGroup, newGroup);
    }
  }

  console.warn(`${total} file(s) changed group ownership`);

  if (CI && total > 0 && !expectingGroupChanges) {
    console.warn(preserveBlankLines(BLOCKING_MESSAGE));
    return 2;
  }

  return 0;
}

try {
  process.exitCode = await main();
} catch (e) {
  // eslint-disable-next-line no-console
  console.error(e);
  process.exitCode = 1;
}
