#!/usr/bin/env python3
#
# Initializes Semgrep's settings file to always suppress its metrics notice.

from pathlib import Path
from textwrap import dedent
from uuid import uuid4


def main():
    semgrep_dir = Path.home() / ".semgrep"
    semgrep_settings = semgrep_dir / "settings.yml"

    if semgrep_settings.exists():
        print("Semgrep settings file already exists; nothing to do")
        return

    settings = dedent(
        f"""\
        has_shown_metrics_notification: true
        anonymous_user_id: {uuid4()}
        """
    )

    semgrep_dir.mkdir(parents=True, exist_ok=True)
    with semgrep_settings.open("w", encoding="UTF-8") as f:
        f.write(settings)


if __name__ == "__main__":
    main()
