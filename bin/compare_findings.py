#!/usr/bin/env python3
import argparse
import json
import logging
import os
import re
import sys
from collections import defaultdict
from contextlib import contextmanager
from itertools import chain
from subprocess import run
from textwrap import dedent, indent

ANSI_BOLD = "\x1b[1m"
ANSI_CLEAR_LINE = "\x1b[0K"
ANSI_CYAN_BOLD = "\x1b[36;1m"
ANSI_RED = "\x1b[31m"
ANSI_UNDERLINE = "\x1b[4m"
ANSI_RESET = "\x1b[0;m"

UTF8 = "UTF-8"


def parse_args():
    argparser = argparse.ArgumentParser(
        description="""
        A rough script to help determine effects of rules changes on findings in the GitLab codebase.
        """,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    default_rules = os.path.normpath(
        os.path.join(os.path.dirname(os.path.realpath(__file__)), "../rules")
    )
    argparser.add_argument(
        "--rules",
        default=default_rules,
        help="The path to the rules file(s).",
    )

    argparser.add_argument(
        "--base",
        help="""
        Commit hash to use as the base rules for comparison.
        Defaults to the the merge base of the current branch with main.
        """,
    )

    argparser.add_argument(
        "--tip",
        help="""
        Commit hash to use as latest version of rules for comparison. In CI
        merge request pipelines, this defaults to the source branch tip.
        Otherwise, it defaults to HEAD.
        """,
    )

    argparser.add_argument(
        "paths", nargs="+", help="The paths to scan (e.g., point at ~/gitlab/{,ee/}app)"
    )

    return argparser.parse_args()


@contextmanager
def checkout_commit(commit: str):
    git = run(
        ["git", "status", "--porcelain"], capture_output=True, encoding=UTF8, check=True
    )

    if git.stdout.strip():
        logging.error("Git workdir isn't clean! Aborting.")
        raise Exception("Git workdir not clean")

    git = run(
        ["git", "rev-parse", "--symbolic", "--abbrev-ref", "@"],
        capture_output=True,
        encoding=UTF8,
        check=True,
    )
    original_ref = git.stdout.strip()

    logging.debug(f"Checking out {commit}...")

    run(["git", "checkout", commit], check=True)

    try:
        yield
    finally:
        logging.debug(f"Reverting to original head {original_ref}...")

        run(
            ["git", "checkout", original_ref],
            capture_output=True,
            encoding=UTF8,
            check=True,
        )


def preserve_blank_lines(string: str):
    """
    Utility to work around https://gitlab.com/gitlab-org/gitlab/-/issues/217231.

    Works by adding whitespace to otherwise blank lines.

    Triple quoted strings with whitespace-only lines is another approach,
    but is less explicit. That would also need textwrap.dedent() for readability,
    but that interferes with some of the rendering we do (e.g., code lines).
    """
    return re.sub("\n(?=\n)", "\n ", string)


class Scan:
    def __init__(
        self,
        paths: str,
        config: str,
    ):
        self.paths = paths
        self.config = config
        self.results = []

        self.scan()

    def scan(self):
        semgrep = run(
            [
                "semgrep",
                "scan",
                "--json",
                f"--config={self.config}",
                *self.paths,
            ],
            capture_output=True,
            encoding=UTF8,
        )

        logging.info(semgrep.stderr)

        if semgrep.returncode == 0:
            self.results: list[dict] = json.loads(semgrep.stdout)["results"]
        else:
            # Semgrep writes some errors to stdout, so redirect it to stderr instead.
            logging.warning(semgrep.stdout)


class Findings:
    """A renderable list of findings suitable for printing to the terminal."""

    def __init__(self, findings: list[dict]):
        self.findings = [Finding(finding) for finding in findings]

    def __str__(self):
        if not self.findings:
            return ""

        return "\n\n\n".join(str(finding) for finding in self.findings)


class Finding:
    """A renderable finding suitable for printing to the terminal."""

    def __init__(self, finding: dict):
        self.finding = finding

    @staticmethod
    def _mark_lines(lines: str, start, end) -> str:
        mark_length = end["offset"] - start["offset"]

        # Columns are 1-based, but strings are 0-based, so subtract one
        mark_start = start["col"] - 1
        mark_end = mark_start + mark_length

        text_before = lines[:mark_start]
        marked = f"{ANSI_RESET}\n{ANSI_RED}".join(
            lines[mark_start:mark_end].split("\n")
        )
        text_after = lines[mark_end:]

        return f"{text_before}{ANSI_RED}{marked}{ANSI_RESET}{text_after}"

    def __str__(self):
        """Returns a string suitable for printing to the terminal, representing the finding."""
        indent_spaces = 2
        max_number_width = 4
        check_id = self.finding["check_id"]
        path = self.finding["path"]
        message = f'{self.finding["extra"]["message"].strip()}'
        message = indent(message, " " * indent_spaces)
        start = self.finding["start"]
        end = self.finding["end"]
        start_line = start["line"]
        marked_lines = self._mark_lines(self.finding["extra"]["lines"], start, end)
        numbered_lines = "\n".join(
            f"{str(start_line + i).rjust(indent_spaces + max_number_width)} ┊ {line}"
            for i, line in enumerate(marked_lines.split("\n"))
        )

        return f"{ANSI_BOLD}{path}:{start_line}{ANSI_RESET}\n{check_id}\n{message}\n\n{numbered_lines}"


def merge_base(target: str, source: str):
    git = run(
        ["git", "merge-base", target, source],
        capture_output=True,
        encoding=UTF8,
        check=True,
    )

    logging.debug(git.stderr)

    return git.stdout.strip()


class Diff:
    def __init__(self, base_sha: str, tip_sha: str):
        self.base_sha = base_sha
        self.tip_sha = tip_sha
        self.base_result: dict[str, dict] = dict()
        self.tip_result: dict[str, dict] = dict()

    def _subtract(self, minuend, subtrahend):
        fingerprints = set(minuend.keys()) - set(subtrahend.keys())
        return [
            finding
            for finding in minuend.values()
            if finding["extra"]["fingerprint"] in fingerprints
        ]

    @property
    def base_total(self):
        return len(self.base_result)

    @property
    def tip_total(self):
        return len(self.tip_result)

    def _to_map(self, findings: list[dict]):
        return dict((finding["extra"]["fingerprint"], finding) for finding in findings)

    def write_artifacts(self):
        with (
            open(f"base_{self.base_sha}.json", "w") as base_file,
            open(f"tip_{self.tip_sha}.json", "w") as tip_file,
        ):
            json.dump(self.base_result, base_file)
            json.dump(self.tip_result, tip_file)

    def add_base_result(self, result: list[dict]):
        self.base_result = self._to_map(result)

    def add_tip_result(self, result: list[dict]):
        self.tip_result = self._to_map(result)

    def _added_dropped_by_location_and_id(self):
        added = self._subtract(self.tip_result, self.base_result)
        dropped = self._subtract(self.base_result, self.tip_result)

        def to_key(_finding):
            return (
                f"{_finding['check_id']}:{_finding['path']}:{_finding['start']['line']}"
            )

        def flatten(iterable):
            return list(chain.from_iterable(iterable))

        added_by_location = defaultdict(list)
        for finding in added:
            added_by_location[to_key(finding)].append(finding)

        dropped_by_location = defaultdict(list)
        for finding in dropped:
            dropped_by_location[to_key(finding)].append(finding)

        for key in added_by_location:
            # Because we're looking at the symmetric intersection,
            # there's no need to iterate over dropped_by_location as well.
            while added_by_location[key] and dropped_by_location[key]:
                # These findings have the same rule id, path and start line,
                # and appear in both the added and dropped lists. They are
                # *probably* the same finding, just that their fingerprint(s)
                # changed.
                added_by_location[key].pop()
                dropped_by_location[key].pop()

        return {
            "added": flatten(added_by_location.values()),
            "dropped": flatten(dropped_by_location.values()),
        }

    def print(self):
        by_location_and_id = self._added_dropped_by_location_and_id()
        added = by_location_and_id["added"]
        dropped = by_location_and_id["dropped"]

        added_findings = f"\n\n\nNew findings:\n\n{Findings(added)}"
        dropped_findings = f"\n\n\nDropped findings:\n\n{Findings(dropped)}"
        summary = dedent(
            f"""\
            Total base findings: {self.base_total}
            Total tip findings: {self.tip_total}
            {len(added)} added findings, {len(dropped)} dropped findings"""
        )

        output = [summary]

        if self.base_total + len(added) - len(dropped) != self.tip_total:
            output.append(
                dedent(
                    f"""\
                    Something doesn't add up:
                    {self.base_total} + {len(added)} - {len(dropped)} != {self.tip_total}

                    Probably the heuristics for duplicate findings are not perfect.
                    Inspect the artifacts for more information."""
                )
            )

        if added:
            output.insert(0, added_findings)

        if dropped:
            output.insert(0, dropped_findings)

        logging.info(preserve_blank_lines("\n\n\n".join(output)))


def base(arg_base: str):
    if arg_base:
        return arg_base

    if mr_base := os.getenv("CI_MERGE_REQUEST_DIFF_BASE_SHA"):
        return mr_base

    return merge_base("main", "@")


def tip(arg_tip: str):
    if arg_tip:
        return arg_tip

    if mr_source_tip := os.getenv("CI_MERGE_REQUEST_SOURCE_BRANCH_SHA"):
        return mr_source_tip

    return "HEAD"


def sha(ref: str):
    git = run(["git", "rev-parse", ref], capture_output=True, encoding=UTF8, check=True)
    logging.debug(git.stderr)
    return git.stdout.strip()


def main() -> int:
    args = parse_args()

    logging.basicConfig(format="%(message)s", level=logging.DEBUG)

    resolved_base = base(args.base)
    resolved_tip = tip(args.tip)
    base_sha = sha(resolved_base)
    tip_sha = sha(resolved_tip)

    logging.debug(f"{base_sha=} {tip_sha=}")

    diff = Diff(base_sha, tip_sha)

    with checkout_commit(base_sha):
        scan = Scan(args.paths, args.rules)
        diff.add_base_result(scan.results)

    with checkout_commit(tip_sha):
        scan = Scan(args.paths, args.rules)
        diff.add_tip_result(scan.results)

    diff.write_artifacts()
    diff.print()

    return 0


if __name__ == "__main__":
    sys.exit(main())
