/**
 * Splits the given lines into three parts: text before the match, the match
 * itself, and the text after the match.
 *
 * @param {Object} start The start field of a finding.
 * @param {Object} end The end field of a finding.
 * @param {string} lines The extra.lines field of a finding.
 * @returns {Array<string>}
 */
export function partitionLines(start, end, lines) {
  const markLength = end.offset - start.offset;

  // Columns are 1-based, but strings are 0-based, so subtract one
  const markStart = start.col - 1;
  const markEnd = markStart + markLength;

  return [lines.slice(0, markStart), lines.slice(markStart, markEnd), lines.slice(markEnd)];
}
