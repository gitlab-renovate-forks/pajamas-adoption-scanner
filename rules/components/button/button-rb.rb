# frozen_string_literal: true

# ruleid: button-element-rb
content_tag(
  :button,
  class: 'gl-button btn btn-default copy-to-clipboard-btn',
  title: 'Copy snippet contents',
  onclick: "copyToClipboard('.blob-content[data-blob-id=\"#{blob.id}\"] > pre')"
) do
  external_snippet_icon('copy-to-clipboard')
end

# ruleid: button-element-rb
button_tag

def button
  # ruleid: okay-button-view-component-rb
  render(Pajamas::ButtonComponent.new(
    variant: :confirm,
  )) do
    text
  end
end

def link_buttons
  # ruleid: okay-button-view-component-rb
  link1 = link_button_to _('Foo'), some_path
  # ruleid: okay-button-view-component-rb
  link2 = link_button_to(
    _('Foo'),
    some_path,
    class: 'foo-bar gl-button'
  )
end
