import get from 'lodash/get.js';
import isBoolean from 'lodash/isBoolean.js';
import { validComponentLabels } from 'shared/component_labels.js';
import { shortId } from 'shared/rules.js';

const SEVERITY_INFO = 'INFO';
const SEVERITY_WARNING = 'WARNING';
const SEVERITY_ERROR = 'ERROR';

const hasMigrationEpic = ({ metadata }) => metadata.epic?.length > 0;

const requireProperty = (path, check, expectedType) => (rule) => {
  const value = get(rule, path);

  if (!check(value)) {
    return `Expected ${expectedType} at ${path}; got ${value}`;
  }

  return false;
};

const warningMustNotHaveMigrationEpic = ({ severity, ...rest }) => {
  if (hasMigrationEpic(rest) && severity === SEVERITY_WARNING) {
    return `Rules with severity ${SEVERITY_WARNING} must not have migration epics`;
  }

  return false;
};

const validNeedsEpicOrWarning = ({ severity, ...rest }) => {
  const hasNeedsEpicOrWarning = Object.hasOwn(rest.metadata, 'needsEpicOrWarning');

  if (hasNeedsEpicOrWarning && rest.metadata.needsEpicOrWarning !== true) {
    return `Invalid value ${rest.metadata.needsEpicOrWarning} for needsEpicOrWarning; must be true, or not exist`;
  }

  if (severity !== SEVERITY_ERROR && hasNeedsEpicOrWarning) {
    return `Only severity ${SEVERITY_ERROR} rules can have needsEpicOrWarning`;
  }

  if (severity === SEVERITY_ERROR && hasNeedsEpicOrWarning && hasMigrationEpic(rest)) {
    return `Rule should not have both needsEpicOrWarning and epic; remove needsEpicOrWarning key`;
  }

  return false;
};

const errorMustHaveMigrationEpic = ({ severity, ...rest }) => {
  if (!hasMigrationEpic(rest) && severity === SEVERITY_ERROR) {
    // TODO: remove this once all errors have migration epics
    // #XXX
    if (rest.metadata.needsEpicOrWarning) return false;

    return `Rules with severity ${SEVERITY_ERROR} must have migration epics`;
  }

  return false;
};

const okayMeansInfo = ({ id, severity }) => {
  const startsWithOkay = shortId(id).startsWith('okay');

  if (severity !== SEVERITY_INFO && startsWithOkay) {
    return 'Only informational rules can start with "okay"';
  }

  if (severity === SEVERITY_INFO && !startsWithOkay) {
    return 'Informational rules must start with "okay"';
  }

  return false;
};

const pajamasCompliantMustBeInfo = ({ severity, metadata }) => {
  const { pajamasCompliant } = metadata;

  // If/when it makes sense to relax this rule, do so. For now, there's a 1:1
  // correspondence between severity and Pajamas compliance.
  if ((severity === SEVERITY_INFO) !== pajamasCompliant) {
    return `Expected pajamasCompliant: ${!pajamasCompliant} given severity ${severity}, got ${pajamasCompliant}`;
  }

  return false;
};

const linters = [
  requireProperty('metadata.pajamasCompliant', isBoolean, 'boolean'),
  requireProperty(
    'metadata.componentLabel',
    (string) => validComponentLabels.has(string),
    'valid label',
  ),
  warningMustNotHaveMigrationEpic,
  errorMustHaveMigrationEpic,
  validNeedsEpicOrWarning,
  okayMeansInfo,
  pajamasCompliantMustBeInfo,
];

export function lintRules(rules) {
  const errors = [];

  for (const rule of rules) {
    for (const linter of linters) {
      let error;
      try {
        error = linter(rule);
      } catch (e) {
        error = `Linter ${linter.name} failed: ${e.message}`;
      }

      if (error) errors.push(`${rule.id}: ${error}`);
    }
  }

  return errors;
}
